const execa = require('execa');
const chalk = require('chalk');

const packages = [
];

const build = async () => {
  for (let i = 0; i < packages.length; i++) {
    console.info(`>> start build module ${chalk.yellow(packages[i])}`);
    const { stderr, stdout } = await execa('yarn', [
      'workspace',
      packages[i],
      'build',
    ]);

    if (stderr) {
      console.log(stderr);
    }

    if (stdout) {
      console.log(stdout);
    }
  }
};

build();
