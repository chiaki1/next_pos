import { getUiExtension } from '@vjcspy/ui-extension';

export interface RouterConfig {
  path: string;
  component: () => any;
}

export class AppRoute {
  static SPLASH = 'splash';
  static HOME = 'home';

  private static _routes: RouterConfig[] = [
    {
      path: `/${AppRoute.SPLASH}`,
      component: () => getUiExtension('SPLASH'),
    },
    {
      path: `/${AppRoute.HOME}`,
      component: () => getUiExtension('HOME_PAGE'),
    },
  ];

  static get routes() {
    return AppRoute._routes;
  }

  static configRouter(routes: RouterConfig[]) {
    routes.forEach((r) => {
      AppRoute._routes = AppRoute._routes.filter((_r) => _r.path !== r.path);
      AppRoute._routes.push(r);
    });
  }
}
