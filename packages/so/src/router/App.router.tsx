import { combineHOC, getUiExtension } from '@vjcspy/ui-extension';
import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import { AppRoute } from './Route';

const AppRouter: React.FC<any> = combineHOC()(() => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          {AppRoute.routes.map((r) => (
            <Route
              path={r.path}
              // @ts-ignore
              element={r.component()}
              key={r.path}
            />
          ))}
          <Route
            path="/"
            element={<Navigate to={`/${AppRoute.SPLASH}`} replace />}
          />
          {/*@ts-ignore*/}
          <Route path="*" element={getUiExtension('URL_REWRITE_PAGE')} />
        </Routes>
      </BrowserRouter>
    </>
  );
});

export default AppRouter;
