import { AppRoute } from '../../router/Route';
import Foo from './pages/Foo';

AppRoute.configRouter([
  {
    path: '/foo',
    component: () => <Foo />,
  },
]);
